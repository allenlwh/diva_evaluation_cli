"""Entry point module: process-chunk

Implements the entry-point by using Python or any other languages.
"""

import os
import re


def entry_point(chunk_id, system_cache_dir):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Process a chunk.

    Args:
        chunk_id (int): Chunk id
        system_cache_dir (str): Path to system cache directory

    """
    raise NotImplementedError("You should implement the entry_point method.")
