#!/bin/bash

bin_dir="/usr/bin"
if [ ! -f $bin_dir/python3.7 ];then
  sudo apt install -y python3.7
fi
if [ -f $bin_dir/python3 ];then
  sudo rm -rf $bin_dir/python3
fi
sudo ln -s $bin_dir/python3.7 $bin_dir/python3
