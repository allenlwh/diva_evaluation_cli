#!/bin/bash
env_dir="python_env"
current_path=`realpath $(dirname $0)`
path_to_env_dir="$current_path/$env_dir"

sudo apt-get install -y python3-dev python3-pip
python3.7 -m pip install virtualenv
if [ -d $path_to_env_dir ];then
  . $path_to_env_dir/bin/activate
else
  virtualenv $path_to_env_dir
  . $path_to_env_dir/bin/activate
  python3.7 -m pip --no-cache-dir install -r $current_path/ActEV_Scorer/requirements.txt
fi
