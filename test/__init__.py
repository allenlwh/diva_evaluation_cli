from diva_evaluation_cli.bin.cli import cli_parser


def test_cli(error_msg):
    try:
        if cli_parser():
            self.fail(error_msg)
    except Exception:
        self.fail(error_msg)
