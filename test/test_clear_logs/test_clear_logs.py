'''
This module implements unit tests of the actev clear-logs command.
'''

import sys
import unittest

from test import test_cli


class TestClearLogs(unittest.TestCase):
    """
    Test the actev clear-logs command.
    """

    def test_clear_logs(self):
        sys.argv = [sys.argv[0], 'clear-logs']
        test_cli('`actev clear-logs` failed.')


if __name__ == '__main__':
    unittest.main()
