MAKE=make

check:
	@(echo "** Running UnitTests **\n")
	(python3 -m unittest test/test_*/*.py)

install:
	python3 diva_evaluation_cli/bin/install.sh
